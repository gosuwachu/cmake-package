cmake_minimum_required(VERSION 2.8)
project(client)

add_subdirectory(${CMAKE_CURRENT_SOURCE_DIR}/../test-lib-bin ${CMAKE_CURRENT_BINARY_DIR}/test-lib)

set(SOURCE "main.cpp")
add_executable(client ${SOURCE})
target_link_libraries(client test_lib)
